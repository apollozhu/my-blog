## extract-text-webpack-plugin
替换为 mini-css-extract-plugin@1.6.2
需要修改webpack.base.config.js

##  webpack-dev-middleware@1.12.2
升级到 webpack-dev-middleware@3.7.2

## sw-precache-webpack-plugin@0.11.4
升级到 sw-precache-webpack-plugin@1.0.0

## 删除多余的包
babel-preset-stage-2
babel-preset-env
@babel/core@7.0.0-beta.47
@babel/plugin-proposal-class-properties
vuepress

## 增加的包
stylus-loader@4.3.3
stylus

## 其它修改的内容
webpack4 不支持启动参数 `--hide-modules`，修改为
```
  stats: {
    modules: false,
    entrypoints: false,
  },
```