'use strict'

export const apibase = '/tapi/';
export const proxybase = '/tserver/';
export const socketServer = process.env.NODE_ENV === 'development' ? 'localhost:7001' : '';
