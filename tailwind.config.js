const plugin = require('tailwindcss/plugin')

module.exports = {
    purge: { content: ['./src/**/*.vue'] },
    darkMode: 'class', // false or 'media' or 'class'
    theme: {
        extend: {},
    },
    variants: {
        extend: {
            padding: ['cursor'],
            margin: ['cursor'],
            borderWidth: ['cursor'],
            textColor: ['cursor', 'focus-within'],
            textOpacity: ['cursor'],
            backgroundOpacity: ['cursor'],
            borderOpacity: ['cursor'],
            backgroundColor: ['cursor'],
            borderColor: ['cursor'],
            boxShadow: ['cursor'],
            opacity: ['cursor'],
            scale: ['cursor'],
            translate: ['cursor'],
            display: ['cursor'],
            ringColor: ['cursor'],
            ringOffsetColor: ['cursor'],
            ringOffsetWidth: ['cursor'],
            ringOpacity: ['cursor'],
            ringWidth: ['cursor'],
        },
        visibility: ['responsive', 'hover', 'focus'],
        backgroundColor: ['responsive', 'first', 'last', 'even', 'odd', 'hover', 'focus']
    },
    plugins: [
        plugin(function ({ addVariant, e }) {
            addVariant('cursor', ({ modifySelectors, separator }) => {
                modifySelectors(({ className }) => {
                    return `.${e(`cursor${separator}${className}`)}:focus-within, .${e(`cursor${separator}${className}`)}:focus, .${e(`cursor${separator}${className}`)}:hover, .${e(`cursor${separator}${className}`)}:focus-within`
                })
            })
        }),
    ],
}
