// translate router.meta.title, be used in breadcrumb sidebar tagsview
export function generateTitle(title) {
  if (!this.$i18n) return title; // not support i18n
  const hasKey = this.$te('route.' + title)
  const translatedTitle = this.$t('route.' + title) // $t :this method from vue-i18n, inject in @/lang/index.js
  if (hasKey) {
    return translatedTitle
  }
  return title
}
export function generateSkinColor(color) {
  if (!this.$i18n) return color; // not support i18n
  const hasKey = this.$te('skin.' + color)
  const translatedColor = this.$t('skin.' + color) // $t :this method from vue-i18n, inject in @/lang/index.js
  if (hasKey) {
    return translatedColor
  }
  return color
}