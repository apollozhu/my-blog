export default {
    default: 'message',
    view: {
        countBase: 'people',
        countAero: 'message',
        countOnto: 'shoppingCard',
        countEvent: 'money',
    }
}