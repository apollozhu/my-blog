import Vue from 'vue'
import Router from 'vue-router'
import { constantRouterMap, asyncRouterMap } from './routes'

Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history', //后端支持可开
    // scrollBehavior: () => ({ y: 0 }),
    routes: constantRouterMap.concat(asyncRouterMap)
  })
}