import request from '@/utils/request'

const isProd = process.env.NODE_ENV === 'production';
const port = process.env.PORT || 6321;
const baseURL = isProd ? process.env.VUE_APP_BASE_URL : `http://localhost:${port}`;

export function fetchArticle(params) {
    const { id } = params;
    console.log("call fetchArticle");
    return request({
        url: `${baseURL}/gitee/issues`,
        method: 'get',
        params: { action: 'article', id }
    })
}
