import Vue from 'vue'

const Layout = () => import('@/layout/Layout.vue')
const _import = (file) => {
  return file.endsWith('.vue') ?
    () => import('@/views/' + file) :
    () => import('@/views/' + file + '/index.vue');
}

/**
 * router自定义配置项
 * hidden: true                   如果设置true则左侧路由菜单隐藏
 * name:'router-name'             <keep-alive>使用必须设置 (must set!!!)
 * meta : {
    role: true                   设置是否有权限
    title: 'title'                当前路由的中文名称
    noCache: true                 如果fasle，页面将不会被缓存(默认为false)
    icon: 'icon-file-text'        当前菜单的图标样式，使用阿里iconfont
  }
 **/

/*
 * 基础路映射
 */
const _401 = _import('errorPage/401.vue')
const _403 = _import('errorPage/403.vue')
const _404 = _import('errorPage/404.vue')
const _500 = _import('errorPage/500.vue')

/*
 * 列表页/首页
 */
const _list = _import('list')

/*
 * 文章页
 */
const _article = _import('article')

export const constantRouterMap = [
  {
    path: '/',
    component: Layout,
    redirect: '/list',
    hidden: true,
    children: [
      { path: 'list', name: 'list', component: _list, meta: { title: 'blog', icon: 'tab' } },
      { path: 'article/:id', name: 'article', component: _article, meta: { title: 'blog', icon: 'tab' } },
    ]
  },
];

export const asyncRouterMap = [
  // 错误页面
  {
    path: '/',
    component: Layout,
    name: 'errorpage',
    hidden: true,
    meta: { title: 'errorpage', icon: '404' },
    redirect: 'noRedirect',// 表示面包屑不需要重定向
    children: [
      { path: '401', component: _401, meta: { title: 'page401' } },
      { path: '403', component: _403, meta: { title: 'page403' } },
      { path: '404', component: _404, meta: { title: 'page404' } },
      { path: '500', component: _500, meta: { title: 'page500' } },
    ]
  },

  { path: '*', redirect: '/404' }
];
