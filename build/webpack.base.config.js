const path = require('path')
const webpack = require('webpack')
// const ExtractTextPlugin = require('extract-text-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const BabiliPlugin = require("babili-webpack-plugin")
// const TerserPlugin = require("terser-webpack-plugin")
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const { VueLoaderPlugin } = require('vue-loader')

const isProd = process.env.NODE_ENV === 'production'

console.log("Now process env: ", process.env.NODE_ENV, process.env.VUE_ENV)

class ServerMiniCssExtractPlugin extends MiniCssExtractPlugin {
  getCssChunkObject(mainChunk) {
    return {};
  }
}

const CssExtractPlugin = process.env.VUE_ENV === 'server' ? ServerMiniCssExtractPlugin : MiniCssExtractPlugin;

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = {
  devtool: isProd
    ? false
    : '#cheap-module-source-map',
  output: {
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/dist/',
    filename: '[name].[chunkhash].js'
  },
  stats: {
    modules: false,
    entrypoints: false,
  },
  resolve: {
    alias: {
      'public': path.resolve(__dirname, '../public'),
      '@': resolve('src')
    }
  },
  module: {
    noParse: /es6-promise\.js$/, // avoid webpack shimming process
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          compilerOptions: {
            preserveWhitespace: false
          }
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        // include: [
        //   resolve('src'),
        //   resolve('node_modules/highlight.js'),
        //   resolve('node_modules/parse5'),
        // ]
        exclude: resolve('node_modules')
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        query: {
          limit: 10000,
          name: 'fonts/[name].[ext]'
        }
      },
      {
        test: /\.css$/,
        // 重要：使用 vue-style-loader 替代 style-loader
        use: isProd
          ? [CssExtractPlugin.loader, 'css-loader', 'postcss-loader']
          : ['vue-style-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.styl(us)?$/,
        use: isProd
          ? [CssExtractPlugin.loader, 'css-loader', 'stylus-loader']
          : ['vue-style-loader', 'css-loader', 'stylus-loader']
      },
      {
        test: /\.scss$/,
        use: isProd
          ? [CssExtractPlugin.loader, 'css-loader', 'sass-loader']
          : ['vue-style-loader', 'css-loader', 'sass-loader']
      },
    ]
  },
  performance: {
    hints: false
  },
  plugins: isProd
    ? [
      new CssExtractPlugin({
        filename: 'common.[chunkhash].css'
      }),
      new VueLoaderPlugin(),
      // new webpack.optimize.UglifyJsPlugin({
      //   compress: { warnings: false },
      // }),
      // new BabiliPlugin(),
      new webpack.optimize.ModuleConcatenationPlugin(),
      // new ExtractTextPlugin({
      //   filename: 'common.[chunkhash].css'
      // })
    ]
    : [
      new VueLoaderPlugin(),
      new FriendlyErrorsPlugin()
    ]
}
