import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import tagsView from './modules/tagsView'
import articles from './modules/articles'
import getters from './getters'

Vue.use(Vuex)

export function createStore() {
  return new Vuex.Store({
    modules: {
      app,
      tagsView,
      articles,
    },
    getters
  })
}
