const dotenv = require('dotenv');
const fs = require('fs')
const path = require('path')

// read env parameters
function readEnv(file) {
    const fullname = path.join(__dirname, file);
    if (fs.existsSync(fullname)) {
        console.log('read ', file)
        const config = dotenv.parse(fs.readFileSync(fullname));
        for (const k in config) {
            process.env[k] = config[k]
        }
        console.log(config)
    }
}

['.env', `.env.${process.env.NODE_ENV}`].forEach(file => {
    readEnv(file);
})

module.exports = dotenv;