# my-blog

最新动态：

2022.1.8 更新到webpack4

**缘由**

这是进入2022年的第一个项目，出发点是多年以前看过有人用github的issue写博客，好像就是umijs的老大，忘记叫啥了，反正是个大佬。于是我也想用vue做一个纯前端的博客，后台用gitee的issue。

但是做的过程中我突然想到博客的初衷是为了让更多的人看到，那就要求必须是**搜索引擎友好**，所以临时又改变思路，开始寻求起vue的ssr解决方案。尤大几年前出了一个例子[HackerNews Demo](https://github.com/vuejs/vue-hackernews-2.0/)，专门用来演示vue的ssr方法，但是由于时间太早了，使用的webpack是3.1的，因此遇到不少问题，这也算开源软件的痼疾吧，就是发展过快，兼容性不足。

花了好几天，终于发布了第一版，虽然有很多功能没有做，但主要目标都达成了，就是：

1. 使用gitee的issue写博客，支持图片、代码高亮、表格等功能

2. 搜索引擎友好，每篇博客都有自己固定的访问URL，页面采用ssr方式，所得即所见

3. 使用vue的elementUI，使得界面基本上具备了现代UI的样子

## 使用方法

准备工作：由于需要使用gitee的api进行issue访问，因此首先需要在gitee上注册一个账号，并申请一个**私人令牌**。

下载代码后在根目录建立一个`.env`文件用于配置参数

```
VUE_APP_BASE_URL=博客服务所在的IP地址和端口号，例如http://localhost:6321
VUE_APP_ACCESS_TOKEN=私人令牌，例如340c78ea67b452c3b54b652ae94d73cd
VUE_APP_REPO_NAME=写博客的项目地址，例如log4j/pig
VUE_APP_BLOG_TITLE=博客标题
VUE_APP_ICP_NO=备案号
```

然后就是标准的node工程处理方法了

```
npm install 或 yarn //安装依赖库

npm run build 或 yarn build //编译

npm run dev 或 yarn dev //启动开发服务模式，支持热更新

npm start 或 yarn start //启动正式服务模式
```

如果是部署在服务器上作为长期服务，可以考虑使用[pm2](https://pm2.keymetrics.io/)来进行服务的启动。

```
pm2 npm start
```

## 附带几张效果图

![list](./snapshots/list.png)

![article](./snapshots/article.png)
