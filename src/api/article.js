import request from '@/utils/request'

export function fetchList(params) {
  console.log('fetch list ', params)
  return request({
    url: `/gitee/issues`,
    method: 'get',
    params: Object.assign({ action: 'list' }, params),
  })
}

export function queryList(params) {
  return request({
    url: `/gitee/issues`,
    method: 'get',
    params: Object.assign({ action: 'query' }, params),
  })
}

export function aggr() {
  return request({
    url: `/gitee/issues`,
    method: 'get',
    params: Object.assign({ action: 'tags' }),
  })
}