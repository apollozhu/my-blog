import Cookies from 'js-cookie'

const app = {
  state: {
    icpNo: 'ICP No',
    blogTitle: '码农的个人博客',
    // 中英文
    language: Cookies.get('language') || 'zh',
    sidebar: {
      opened: !+Cookies.get('sidebarStatus')
    }
  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1)
      } else {
        Cookies.set('sidebarStatus', 0)
      }
      state.sidebar.opened = !state.sidebar.opened
    },
    // 中英文
    SET_LANGUAGE: (state, language) => {
      state.language = language
      Cookies.set('language', language)
    },
    GET_COMM_INFO: state => {
      const { VUE_APP_ICP_NO: icp_no, VUE_APP_BLOG_TITLE: blog_title } = process ? process.env : {};
      if (icp_no) state.icpNo = icp_no;
      if (blog_title) state.blogTitle = blog_title;
    }
  },
  actions: {
    getCommInfo: ({ commit }) => {
      return new Promise(resolve => {
        resolve(commit('GET_COMM_INFO'))
      });
    },
    ToggleSideBar: ({ commit }) => {
      commit('TOGGLE_SIDEBAR')
    },
    // 中英文
    setLanguage({ commit }, language) {
      commit('SET_LANGUAGE', language)
    }
  }
}

export default app
