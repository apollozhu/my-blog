const getters = {
  accessToken: state => state.app.accessToken,
  repoName: state => state.app.repoName,
  icpNo: state => state.app.icpNo,
  blogTitle: state => state.app.blogTitle,
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  // visitedViews: state => state.app.visitedViews,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  isRouterAlive: state => state.tagsView.isRouterAlive,
  articles: state => state.articles.list,
  searchLabel: state => state.articles.searchLabel,
  searchKey: state => state.articles.searchKey,
  showList: state => state.articles.showList,
}
export default getters
