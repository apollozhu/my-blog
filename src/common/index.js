import svgMapper from './svg'

export default {
    svg: (key) => {
        const arr = key.split('.')
        let c = svgMapper
        let found = true;
        for (let t of arr) {
            if (t in c) c = c[t]
            else {
                found = false;
                break;
            }
        }
        if (found) return c
        return svgMapper['default'];
    }
}