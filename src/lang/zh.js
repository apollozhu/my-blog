
export default {
  // 导航栏
  navbar: {
    title: '自动化管理系统',
    languageSwitch: '语言切换',
    theme: '主题'
  },
  skin: {
    Blue: '天空蓝',
    Green: '典雅绿',
    Red: '樱桃红',
    Purple: '贵族紫',
    Default: '默认'
  },
  route: {
    AliIcons: '阿里图标',
    contextmenu: '右键菜单',
    divier: '分割线',
    group: '按钮组',
    submenu: '子菜单',
    disabled: '禁用',
    customtrigger: '自定义事件',
    Icons: 'icon图标',
    calendar: '日历',
    Upload: '文件上传',
    Components: '组件',
    dragKanban: '可拖拽看板',
    backToTop: '返回顶部',

    dashboard: '首页',

    spider: '爬虫管理（外网）',
    monitor: '任务监控',
    es: '数据查看',

    infoSearch: '信息检索（内网）',
    infoBase: '原始报文库',
    infoAero: '航天信息库',
    infoOnto: '领域本体库',

    anaApp: '智能分析（内网）',
    appClassify: '分类统计',
    appBehavior: '行为分析',
    appEarlyWarning: '预警分析',
    appHotSpot: '热点分析',
    appEventTimeline: '时间线分析',

    errorpage: '错误页面',
    page401: '401页面',
    page403: '403页面',
    page404: '404页面',
    page500: '500页面',

    basic: '基础组件',
    tab: 'table选项卡',
    complextable: '综合Table',
    ruleform: '表单',
    tree: '树形菜单',

    charts: '图表组件',
    keyboard: '键盘图',
    linemarker: '折线图',
    mixchart: '混合图',
    basiccharts: '基础图形',

    extends: '扩展组件',
    backtotop: '返回顶端',
    splitpanel: '分隔区块',
    mixin: '界面特效',

    vendor: '第三方组件',
    fullcalendar: '日历 v5',
    fullcalendar_alt: '日历 v3',
    qiniu: '七牛',
    quilleditor: 'quill编辑器',
    tinymceeditor: 'Tinymce编辑器',
    richtext: '富文本',

    miscellaneous: '杂项',
    exportexcel: '导出为Excel',
    svgicon: 'svg图标',
    i18n: '国际化',

    helpdocs: '帮助文档',

    blog: '码农的个人博客',
  },
  permission: {
    roles: '你的权限',
    switchRoles: '切换权限'
  },
  excel: {
    export: '导出',
    selectedExport: '导出已选择项',
    placeholder: '请输入文件名(默认excel-list)'
  },
  spider: {
    addTask: '新任务',
    refreshList: '刷新列表',
    setTask: '配置任务',
  },
  view: {
    countBase: '原始报文数量',
    countAero: '航天器数量',
    countOnto: '领域本体数量',
    countEvent: '事件数量',
  },
  satProps: {
    aliasName: '卫星别名',
    officialName: '卫星名称',
    country: '所属国家/地区/组织',
    ownerCountry: '运营方所属国家/地区',
    owner: '运营方',
    users: '用户类别',
    purpose: '载荷类型',
    detailedPurpose: '具体用途',
    orbit: '轨道分类',
    orbitType: 'Type of Orbit',
    longitude: '同步定位点',
    perigee: '近地点',
    apogee: '远地点',
    eccentricity: '偏心率',
    inclination: '倾角',
    period: '周期',
    launchMass: '发射重量',
    dryMass: '净重',
    power: '功率',
    launchDate: '发射日期',
    lifetime: '设计寿命',
    contractor: '承包商名称',
    contractorCountry: '承包商所属国家/地区',
    launchSite: '发射场名称',
    launchVehicle: '运载火箭名称',
    cospar: '国际编号',
    norad: 'NORAD编号',
    comments: '备注',
    sourceForOribit: 'Source Used for Orbital Data',
    source: '信息来源',
    objectType: '目标类型',
    decayDate: '陨落日期',
    rcsSize: 'RCS尺寸',
    equipment: '主要星载设备',
    configuration: '卫星平台',
    propulsion: '推进器',
    description: '卫星简介',
    remarks: 'Remarks',
  }
}
